--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: radius; Tablespace: 
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO radius;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: radius
--

CREATE SEQUENCE django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO radius;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: radius
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: radacct; Type: TABLE; Schema: public; Owner: radius; Tablespace: 
--

CREATE TABLE radacct (
    radacctid bigint NOT NULL,
    acctsessionid character varying(64) NOT NULL,
    acctuniqueid character varying(32) NOT NULL,
    username character varying(253),
    groupname character varying(253),
    realm character varying(64),
    nasipaddress inet NOT NULL,
    nasportid character varying(15),
    nasporttype character varying(32),
    acctstarttime timestamp with time zone,
    acctstoptime timestamp with time zone,
    acctsessiontime bigint,
    acctauthentic character varying(32),
    connectinfo_start character varying(50),
    connectinfo_stop character varying(50),
    acctinputoctets bigint,
    acctoutputoctets bigint,
    calledstationid character varying(50),
    callingstationid character varying(50),
    acctterminatecause character varying(32),
    servicetype character varying(32),
    xascendsessionsvrkey character varying(10),
    framedprotocol character varying(32),
    framedipaddress inet,
    acctstartdelay integer,
    acctstopdelay integer
);


ALTER TABLE public.radacct OWNER TO radius;

--
-- Name: radacct_radacctid_seq; Type: SEQUENCE; Schema: public; Owner: radius
--

CREATE SEQUENCE radacct_radacctid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.radacct_radacctid_seq OWNER TO radius;

--
-- Name: radacct_radacctid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: radius
--

ALTER SEQUENCE radacct_radacctid_seq OWNED BY radacct.radacctid;


--
-- Name: radcheck; Type: TABLE; Schema: public; Owner: radius; Tablespace: 
--

CREATE TABLE radcheck (
    id integer NOT NULL,
    username character varying(64) DEFAULT ''::character varying NOT NULL,
    attribute character varying(64) DEFAULT ''::character varying NOT NULL,
    op character(2) DEFAULT '=='::bpchar NOT NULL,
    value character varying(253) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.radcheck OWNER TO radius;

--
-- Name: radcheck_id_seq; Type: SEQUENCE; Schema: public; Owner: radius
--

CREATE SEQUENCE radcheck_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.radcheck_id_seq OWNER TO radius;

--
-- Name: radcheck_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: radius
--

ALTER SEQUENCE radcheck_id_seq OWNED BY radcheck.id;


--
-- Name: radgroupcheck; Type: TABLE; Schema: public; Owner: radius; Tablespace: 
--

CREATE TABLE radgroupcheck (
    id integer NOT NULL,
    groupname character varying(64) DEFAULT ''::character varying NOT NULL,
    attribute character varying(64) DEFAULT ''::character varying NOT NULL,
    op character(2) DEFAULT '=='::bpchar NOT NULL,
    value character varying(253) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.radgroupcheck OWNER TO radius;

--
-- Name: radgroupcheck_id_seq; Type: SEQUENCE; Schema: public; Owner: radius
--

CREATE SEQUENCE radgroupcheck_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.radgroupcheck_id_seq OWNER TO radius;

--
-- Name: radgroupcheck_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: radius
--

ALTER SEQUENCE radgroupcheck_id_seq OWNED BY radgroupcheck.id;


--
-- Name: radgroupreply; Type: TABLE; Schema: public; Owner: radius; Tablespace: 
--

CREATE TABLE radgroupreply (
    id integer NOT NULL,
    groupname character varying(64) DEFAULT ''::character varying NOT NULL,
    attribute character varying(64) DEFAULT ''::character varying NOT NULL,
    op character(2) DEFAULT '='::bpchar NOT NULL,
    value character varying(253) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.radgroupreply OWNER TO radius;

--
-- Name: radgroupreply_id_seq; Type: SEQUENCE; Schema: public; Owner: radius
--

CREATE SEQUENCE radgroupreply_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.radgroupreply_id_seq OWNER TO radius;

--
-- Name: radgroupreply_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: radius
--

ALTER SEQUENCE radgroupreply_id_seq OWNED BY radgroupreply.id;


--
-- Name: radpostauth; Type: TABLE; Schema: public; Owner: radius; Tablespace: 
--

CREATE TABLE radpostauth (
    id bigint NOT NULL,
    username character varying(253) NOT NULL,
    pass character varying(128),
    reply character varying(32),
    calledstationid character varying(50),
    callingstationid character varying(50),
    authdate timestamp with time zone DEFAULT '2016-04-25 15:54:28.197126-04'::timestamp with time zone NOT NULL
);


ALTER TABLE public.radpostauth OWNER TO radius;

--
-- Name: radpostauth_id_seq; Type: SEQUENCE; Schema: public; Owner: radius
--

CREATE SEQUENCE radpostauth_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.radpostauth_id_seq OWNER TO radius;

--
-- Name: radpostauth_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: radius
--

ALTER SEQUENCE radpostauth_id_seq OWNED BY radpostauth.id;


--
-- Name: radreply; Type: TABLE; Schema: public; Owner: radius; Tablespace: 
--

CREATE TABLE radreply (
    id integer NOT NULL,
    username character varying(64) DEFAULT ''::character varying NOT NULL,
    attribute character varying(64) DEFAULT ''::character varying NOT NULL,
    op character(2) DEFAULT '='::bpchar NOT NULL,
    value character varying(253) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.radreply OWNER TO radius;

--
-- Name: radreply_id_seq; Type: SEQUENCE; Schema: public; Owner: radius
--

CREATE SEQUENCE radreply_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.radreply_id_seq OWNER TO radius;

--
-- Name: radreply_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: radius
--

ALTER SEQUENCE radreply_id_seq OWNED BY radreply.id;


--
-- Name: radusergroup; Type: TABLE; Schema: public; Owner: radius; Tablespace: 
--

CREATE TABLE radusergroup (
    username character varying(64) DEFAULT ''::character varying NOT NULL,
    groupname character varying(64) DEFAULT ''::character varying NOT NULL,
    priority integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.radusergroup OWNER TO radius;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: radius
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: radacctid; Type: DEFAULT; Schema: public; Owner: radius
--

ALTER TABLE ONLY radacct ALTER COLUMN radacctid SET DEFAULT nextval('radacct_radacctid_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: radius
--

ALTER TABLE ONLY radcheck ALTER COLUMN id SET DEFAULT nextval('radcheck_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: radius
--

ALTER TABLE ONLY radgroupcheck ALTER COLUMN id SET DEFAULT nextval('radgroupcheck_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: radius
--

ALTER TABLE ONLY radgroupreply ALTER COLUMN id SET DEFAULT nextval('radgroupreply_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: radius
--

ALTER TABLE ONLY radpostauth ALTER COLUMN id SET DEFAULT nextval('radpostauth_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: radius
--

ALTER TABLE ONLY radreply ALTER COLUMN id SET DEFAULT nextval('radreply_id_seq'::regclass);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: radius
--

COPY django_migrations (id, app, name, applied) FROM stdin;
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: radius
--

SELECT pg_catalog.setval('django_migrations_id_seq', 1, false);


--
-- Data for Name: radacct; Type: TABLE DATA; Schema: public; Owner: radius
--

COPY radacct (radacctid, acctsessionid, acctuniqueid, username, groupname, realm, nasipaddress, nasportid, nasporttype, acctstarttime, acctstoptime, acctsessiontime, acctauthentic, connectinfo_start, connectinfo_stop, acctinputoctets, acctoutputoctets, calledstationid, callingstationid, acctterminatecause, servicetype, xascendsessionsvrkey, framedprotocol, framedipaddress, acctstartdelay, acctstopdelay) FROM stdin;
1	80800000	09637e993414c01c	sqltest	\N	\N	192.168.99.88	2155872256	Wireless-802.11	2016-04-25 20:17:40-04	\N	\N			\N	\N	\N	radius-hs	8C:BE:BE:64:89:84	\N				192.168.0.250	0	\N
\.


--
-- Name: radacct_radacctid_seq; Type: SEQUENCE SET; Schema: public; Owner: radius
--

SELECT pg_catalog.setval('radacct_radacctid_seq', 1, true);


--
-- Data for Name: radcheck; Type: TABLE DATA; Schema: public; Owner: radius
--

COPY radcheck (id, username, attribute, op, value) FROM stdin;
1	sqltest	Password	==	testpwd
\.


--
-- Name: radcheck_id_seq; Type: SEQUENCE SET; Schema: public; Owner: radius
--

SELECT pg_catalog.setval('radcheck_id_seq', 1, true);


--
-- Data for Name: radgroupcheck; Type: TABLE DATA; Schema: public; Owner: radius
--

COPY radgroupcheck (id, groupname, attribute, op, value) FROM stdin;
\.


--
-- Name: radgroupcheck_id_seq; Type: SEQUENCE SET; Schema: public; Owner: radius
--

SELECT pg_catalog.setval('radgroupcheck_id_seq', 1, false);


--
-- Data for Name: radgroupreply; Type: TABLE DATA; Schema: public; Owner: radius
--

COPY radgroupreply (id, groupname, attribute, op, value) FROM stdin;
\.


--
-- Name: radgroupreply_id_seq; Type: SEQUENCE SET; Schema: public; Owner: radius
--

SELECT pg_catalog.setval('radgroupreply_id_seq', 1, false);


--
-- Data for Name: radpostauth; Type: TABLE DATA; Schema: public; Owner: radius
--

COPY radpostauth (id, username, pass, reply, calledstationid, callingstationid, authdate) FROM stdin;
1	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 16:08:55.532803-04
2		Chap-Password	Access-Reject	\N	\N	2016-04-25 17:36:50.519207-04
3	wichert	Chap-Password	Access-Reject	\N	\N	2016-04-25 17:51:19.624468-04
4	wichert	Chap-Password	Access-Reject	\N	\N	2016-04-25 17:51:37.029589-04
5	wichert	N=7E=95qUn	Access-Reject	\N	\N	2016-04-25 17:52:11.358169-04
6	sqltest	=CB=16=FB=EF=08=E1=E7	Access-Reject	\N	\N	2016-04-25 17:53:38.176406-04
7	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:54:34.595882-04
8	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:54:39.31997-04
9	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:54:40.438922-04
10	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:54:41.366895-04
11	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:54:42.112098-04
12	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:54:42.981101-04
13	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:54:43.60313-04
14	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:54:44.236869-04
15	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:54:45.215448-04
16	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:58:58.238483-04
17	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:58:58.852932-04
18	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:58:59.328782-04
19	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:58:59.806977-04
20	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 17:59:00.302214-04
21	sqltest	testpwd	Access-Accept	\N	\N	2016-04-25 18:08:30.119906-04
22	sqltest	Chap-Password	Access-Accept	\N	\N	2016-04-25 20:17:40.327811-04
\.


--
-- Name: radpostauth_id_seq; Type: SEQUENCE SET; Schema: public; Owner: radius
--

SELECT pg_catalog.setval('radpostauth_id_seq', 22, true);


--
-- Data for Name: radreply; Type: TABLE DATA; Schema: public; Owner: radius
--

COPY radreply (id, username, attribute, op, value) FROM stdin;
\.


--
-- Name: radreply_id_seq; Type: SEQUENCE SET; Schema: public; Owner: radius
--

SELECT pg_catalog.setval('radreply_id_seq', 1, false);


--
-- Data for Name: radusergroup; Type: TABLE DATA; Schema: public; Owner: radius
--

COPY radusergroup (username, groupname, priority) FROM stdin;
\.


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: radius; Tablespace: 
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: radacct_pkey; Type: CONSTRAINT; Schema: public; Owner: radius; Tablespace: 
--

ALTER TABLE ONLY radacct
    ADD CONSTRAINT radacct_pkey PRIMARY KEY (radacctid);


--
-- Name: radcheck_pkey; Type: CONSTRAINT; Schema: public; Owner: radius; Tablespace: 
--

ALTER TABLE ONLY radcheck
    ADD CONSTRAINT radcheck_pkey PRIMARY KEY (id);


--
-- Name: radgroupcheck_pkey; Type: CONSTRAINT; Schema: public; Owner: radius; Tablespace: 
--

ALTER TABLE ONLY radgroupcheck
    ADD CONSTRAINT radgroupcheck_pkey PRIMARY KEY (id);


--
-- Name: radgroupreply_pkey; Type: CONSTRAINT; Schema: public; Owner: radius; Tablespace: 
--

ALTER TABLE ONLY radgroupreply
    ADD CONSTRAINT radgroupreply_pkey PRIMARY KEY (id);


--
-- Name: radpostauth_pkey; Type: CONSTRAINT; Schema: public; Owner: radius; Tablespace: 
--

ALTER TABLE ONLY radpostauth
    ADD CONSTRAINT radpostauth_pkey PRIMARY KEY (id);


--
-- Name: radreply_pkey; Type: CONSTRAINT; Schema: public; Owner: radius; Tablespace: 
--

ALTER TABLE ONLY radreply
    ADD CONSTRAINT radreply_pkey PRIMARY KEY (id);


--
-- Name: radacct_active_user_idx; Type: INDEX; Schema: public; Owner: radius; Tablespace: 
--

CREATE INDEX radacct_active_user_idx ON radacct USING btree (username, nasipaddress, acctsessionid) WHERE (acctstoptime IS NULL);


--
-- Name: radacct_start_user_idx; Type: INDEX; Schema: public; Owner: radius; Tablespace: 
--

CREATE INDEX radacct_start_user_idx ON radacct USING btree (acctstarttime, username);


--
-- Name: radcheck_username; Type: INDEX; Schema: public; Owner: radius; Tablespace: 
--

CREATE INDEX radcheck_username ON radcheck USING btree (username, attribute);


--
-- Name: radgroupcheck_groupname; Type: INDEX; Schema: public; Owner: radius; Tablespace: 
--

CREATE INDEX radgroupcheck_groupname ON radgroupcheck USING btree (groupname, attribute);


--
-- Name: radgroupreply_groupname; Type: INDEX; Schema: public; Owner: radius; Tablespace: 
--

CREATE INDEX radgroupreply_groupname ON radgroupreply USING btree (groupname, attribute);


--
-- Name: radreply_username; Type: INDEX; Schema: public; Owner: radius; Tablespace: 
--

CREATE INDEX radreply_username ON radreply USING btree (username, attribute);


--
-- Name: radusergroup_username; Type: INDEX; Schema: public; Owner: radius; Tablespace: 
--

CREATE INDEX radusergroup_username ON radusergroup USING btree (username);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

