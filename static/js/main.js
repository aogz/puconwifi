$(document).ready(function(){
    $('#show_form').on('click', function(){
        $(this).hide();
        $('#facebook_login').hide();
        $('#hidden_form').show();
        $('#cancel_form').show()
    });

    $('#cancel_form').on('click', function(){
        $(this).hide();
        $('#facebook_login').show();
        $('#show_form').show();
        $('#hidden_form').hide();
    })

    var counter = 10;
    var interval = setInterval(function() {
        $('#timer').text(counter);
        if (counter == 0) {
            clearInterval(interval);
            $('#advertisment').fadeOut();
        }
        counter--;
    }, 1000);

    var errors = [];
    $('#login_via_email').on('click', function(event){
        for (var i=0; i<errors.length; i++){
            errors[i].close();
        }
        errors = [];

        var first_name = $('#first_name').val();
        if (first_name.length < 2) {
            var error = noty({text: 'Please, provide valid first name', type: 'error'});
            errors.push(error)
        }
        var last_name = $('#last_name').val();
        if (last_name.length < 2) {
            var error = noty({text: 'Please, provide valid last name', type: 'error'});
            errors.push(error)
        }
        var phone = $('#phone').val();
        if (phone.length < 5) {
            var error = noty({text: 'Please, provide valid phone', type: 'error'});
            errors.push(error)
        }
        var email = $('#email').val();
        if (!validateEmail(email)) {
            var error = noty({text: 'Please, provide valid email', type: 'error'});
            errors.push(error)
        }

        var mac = $('#mac').val();
        var uamport = $('#uamport').val();
        var uamip = $('#uamip').val();


        if (errors.length == 0){
            var loading = noty({text: 'Loading', type: 'default'});
            $.post('/connect_user_via_email/',
                {first_name: first_name, last_name: last_name, phone: phone, email: email, mac: mac, uamip: uamip, uamport:uamport},
                function(response){
                    var r = JSON.parse(response);
                    loading.close();
                    noty({text: r.message, type: r.status});
                    if (r.status != 'error'){
                        setTimeout(function(){
                            location.href = "/success/"
                        }, 2000)
                    }
                })
        }
    })

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
})