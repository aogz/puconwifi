import pyrad.packet
from pyrad.client import Client
from pyrad.dictionary import Dictionary

from django.conf import settings
from django.contrib.auth.models import User


class RadiusBackend:
    """
    Authenticate against a RADIUS Server.

    You must have a working RADIUS Server and Secret
    configured in settings.py. For example:

    RADIUS_SERVER = '127.0.0.1'
    RADIUS_SECRET = 'testing123'
    """
    def authenticate(self, username=None, password=None):

        srv = Client(server=settings.RADIUS_SERVER, secret=settings.RADIUS_SECRET, dict=Dictionary("/usr/share/pyrad/dictionary"))

        req = srv.CreateAuthPacket(code=pyrad.packet.AccessRequest)
        req["User-Name"] = username
        req["User-Password"] = req.PwCrypt(password)
        # req["NAS-Identifier"] = "django"

        reply = srv.SendPacket(req)
        if reply.code == pyrad.packet.AccessAccept:
            print "access accepted"
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                # Create a new user. Note that we can set password
                # to anything, because it won't be checked; the password
                # configured on the RADIUS server will.
                user = User(username=username, password='Koh8oF7eiRou4xahxoob')
                user.is_staff = False
                user.is_superuser = False
                user.save()
            return user
        else:
            print "access denied"
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None