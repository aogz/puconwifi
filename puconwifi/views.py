import os
import json
import mailchimp
import random
import requests

import pyrad.packet
from pyrad.client import Client
from pyrad.dictionary import Dictionary

from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
from django.db import connection
from django.conf import settings
from app.models import User


def login(request):
    logout(request)
    images = [f for f in os.listdir(os.path.join(settings.BASE_DIR, 'static/ads'))
              if os.path.isfile(os.path.join(settings.BASE_DIR, 'static/ads', f))]

    image = random.choice(images)
    mac = request.GET.get('mac', '')
    uamport = request.GET.get('uamport', '')
    uamip = request.GET.get('uamip', '')
    return render(request, 'client-login.html', locals())


def success(request):
    return render(request, 'success-login.html', locals())


@csrf_exempt
def connect_user_via_email(request):
    if request.method == 'POST':
        cursor = connection.cursor()
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        mac = request.POST.get('mac')
        ip = request.POST.get('uamip')
        port = request.POST.get('uamport')
        print request.POST

        try:
            _user = User.objects.get(mac=mac)
        except User.DoesNotExist:
            password = User.objects.make_random_password()
            _user = User(username=mac, phone=phone, first_name=first_name, last_name=last_name, email=email, mac=mac)
            _user.set_password(password)
            _user.save()
            cursor.execute("INSERT INTO radcheck (UserName, Attribute, Value) VALUES ('{0}', 'Password', '{1}');"
                           .format(mac, password))
            connection.commit()
            connection.close()
        else:
            cursor.execute("SELECT Value FROM radcheck WHERE UserName='{0}' AND Attribute='Password'".format(mac))
            password = cursor.fetchone()[0]

        try:
            mailchimp_contact_add(_user.username)
        except:
            pass

        try:
            response = requests.get("http://{0}:{1}/logon?username={2}&password={3}".format(ip, port, mac, password))
        except:
            return HttpResponse(json.dumps({'status': 'error', 'message': 'Error while connecting to {0}.'.format(ip)}))

        # if radius_authentication(mac, password):
        return HttpResponse(json.dumps({'status': 'success', 'message': 'Successfuly logged in.'}))
        # else:
        #     return HttpResponse(json.dumps({'status': 'error', 'message': 'Internal server error (#RAD).'}))


def connect_user_via_facebook(request):
    mac, ip, port = request.GET.get('params', 'x,x,x').split(',')

    cursor = connection.cursor()

    if request.user.email:
        request.user.username = request.user.email
        request.user.save()

    cursor.execute("SELECT Value FROM radcheck WHERE UserName='{0}' AND Attribute='Password'"
                   .format(mac))
    _password = cursor.fetchone()

    if not _password:
        _password = User.objects.make_random_password()
        request.user.set_password(_password)

        request.user.save()
        cursor.execute("INSERT INTO radcheck (UserName, Attribute, Value) VALUES ('{0}', 'Password', '{1}');"
                           .format(mac, _password))
        connection.commit()
    else:
        _password = _password[0]


    try:
        mailchimp_contact_add(request.user.username)
    except:
        pass

    try:
        response = requests.get("http://{0}:{1}/logon?username={2}&password={3}".format(ip, port, mac, _password))
    except:
        return redirect("http://{0}:{1}/logon?username={2}&password={3}".format(ip, port, mac, _password))


    # if radius_authentication(mac, _password):
    return redirect('/success/')
    # else:
    #     return redirect('/login/')


def radius_authentication(login, password):
    print settings.RADIUS_SERVER, settings.RADIUS_SECRET
    client = Client(server=settings.RADIUS_SERVER, secret=settings.RADIUS_SECRET,
                    dict=Dictionary(settings.RADIUS_DICTIONARY))
    req = client.CreateAuthPacket(code=pyrad.packet.AccessRequest, User_Name=login, NAS_Identifier="Wifi Pucon")
    req["User-Password"] = req.PwCrypt(password)

    reply = client.SendPacket(req)
    return reply.code == pyrad.packet.AccessAccept


def mailchimp_contact_add(email):
    api = mailchimp.Mailchimp(settings.MAILCHIMP_API_KEY)
    api.lists.subscribe(settings.MAILCHIMP_LIST_ID, {'email': email})
