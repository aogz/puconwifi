# -*- coding: utf-8 -*-
from django.shortcuts import redirect, HttpResponse
from social.apps.django_app.middleware import SocialAuthExceptionMiddleware
from social.exceptions import AuthCanceled, AuthAlreadyAssociated


class AuthCancelledExceptionMiddleware(SocialAuthExceptionMiddleware):
    def process_exception(self, request, exception):
        if type(exception) == AuthCanceled:
            return redirect('/login')
        elif type(exception) == AuthAlreadyAssociated:
            return HttpResponse('/')
        else:
            raise exception